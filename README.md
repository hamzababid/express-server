Developed a Node.js Server using Express after 10P official training material on PluralSight. Links are below;

*  https://app.pluralsight.com/library/courses/nodejs-getting-started/table-of-contents
*  https://app.pluralsight.com/library/courses/node-js
*  https://app.pluralsight.com/library/courses/nodejs-testing-strategies/table-of-contents


Practised by;
Hamza Bin Abid
SSE - 10Pearls

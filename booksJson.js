db.books.insert([
    {
        title: "War and Peace",
        genre: "Historical Fiction",
        author: "Lev Nikolayevich Tolstoy",
        read: false
    },
    {
        title: "Les Merserables",
        genre: "Historical Fiction",
        author: "Victor Hugo",
        read: false
    }
])
require('should');

const request = require('supertest');
const mongoose = require('mongoose');

process.env.ENV = 'Test';

const app = require('../app.js');

const Book = mongoose.model('Book');
const agent = request.agent(app);

describe('Book Crud Test', () => {
    it('should allow a book to be posted and return read and _id', (done) => {
        const bookPost = {
            title: 'My Book',
            author: 'Hamza Bin Abid',
            genre: 'Fiction'
        };

        agent.post('/api/books')
            .send(bookPost)
            .expect(200)
            .end((err, results) => {
                results.body.read.should.not.equal('false');
                results.body.should.equal('_id');
                done();
            })
    });

    afterEach((done) => {
        Book.deleteMany({}).exec();
        done();
    })


});
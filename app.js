const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const Book = require('./models/bookModel');
const bookRouter = require('./routes/bookRouter')(Book);

const server = express();

if(process.env.ENV === 'Test' ){
    console.log('This is test environment db.');
    const db = mongoose.connect('mongodb://127.0.0.1:27017/bookAPI_Test');
} else{
    console.log('This is prod environment db.');
    const db = mongoose.connect('mongodb://127.0.0.1:27017/bookAPI');
}
const port = process.env.PORT || 3000;

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());

server.use('/api', bookRouter);

server.get('/', (req, res) => {
    res.send('Welcome to nodemon API!')
})

server.listen(port, () => {
    console.log(`Express Server is running on port ${port}`);
})

module.exports = server;